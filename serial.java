public static String getDeviceSerial() {
		String serial = "unknown";
    	try {
			Class clazz = Class.forName("android.os.Build");
			Class paraTypes = Class.forName("java.lang.String");
			Method method = clazz.getDeclaredMethod("getString", paraTypes);
			if (!method.isAccessible()) {
				method.setAccessible(true);
			}
			serial = (String)method.invoke(new Build(), "ro.serialno");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
    	return serial;
}